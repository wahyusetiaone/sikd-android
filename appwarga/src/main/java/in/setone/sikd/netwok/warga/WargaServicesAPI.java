package in.setone.sikd.netwok.warga;

import in.setone.sikd.model.warga.login.Response;
import retrofit2.Call;
import retrofit2.http.*;

public interface WargaServicesAPI {

    @FormUrlEncoded
    @POST("login")
    Call<Response> login(
            @Field("kk") String kk,
            @Field("nik") String nik
    );

    @FormUrlEncoded
    @POST("get")
    Call<in.setone.sikd.model.warga.user.Response> userInformation(
            @Field("nik") String nik
    );

    @FormUrlEncoded
    @POST("history")
    Call<in.setone.sikd.model.warga.history.Response> history(
            @Field("nik") String nik
    );

    @FormUrlEncoded
    @POST("request")
    Call<in.setone.sikd.model.warga.request.Response> request(
            @Field("nik") String nik,
            @Field("surat") int surat,
            @Field("keperluan") String keperluan
    );

    @FormUrlEncoded
    @POST("requestv2")
    Call<in.setone.sikd.model.warga.requestv2.Response> requestv2(
            @Field("nik") String nik,
            @Field("surat") int surat,
            @Field("keperluan") String keperluan,
            @Field("hsv") String hsv
    );

//
//    @FormUrlEncoded
//    @POST("register")
//    Call<in.setone.valent.siswa.data.registration.model.CallbackResponse> registration(
//            @Field("username") String username,
//            @Field("password") String password,
//            @Field("nama") String nama,
//            @Field("alamat") String alamat,
//            @Field("no_hp") String no_hp,
//            @Field("jenjang") String jenjang,
//            @Field("kelas") String kelas,
//            @Field("semester") String semester
//    );
//
//    @GET(" ")
//    Call<UserData> getUserData();
//
//    @FormUrlEncoded
//    @POST("jadwal/bySiswa")
//    Call<in.setone.valent.siswa.data.schedule.model.CallbackResponse> jadwalSiswa(
//            @Field("id_siswa") int id_siswa
//    );
//
//    @FormUrlEncoded
//    @POST("jadwal")
//    Call<in.setone.valent.siswa.data.home.model.jadwal.CallbackResponse> jadwalAll(
//            @Field("findBy") int findBy,
//            @Field("kelas") int kelas,
//            @Field("guru") String guru,
//            @Field("matpel") String matpel
//    );
//
//    @FormUrlEncoded
//    @POST("paket")
//    Call<in.setone.valent.siswa.data.home.model.paket.CallbackResponse> paketAll(
//            @Field("findBy") int findBy,
//            @Field("nama_paket") String nama_paket,
//            @Field("nama_guru") String nama_guru
//    );

//    @GET("discover/movie")
//    Call<MoviesResponseModel> getDataMovies(
//            @Query("api_key") String apiKey,
//            @Query("language") String language
//    );
//
//    @GET("discover/tv")
//    Call<TvResponseModel> getDataTv(
//            @Query("api_key") String apiKey,
//            @Query("language") String language
//    );
//
//    @GET("search/movie")
//    Call<MoviesResponseModel> searchMovie(
//            @Query("api_key") String apiKey,
//            @Query("language") String language,
//            @Query("query") String query
//    );
//
//    @GET("search/tv")
//    Call<TvResponseModel> searchTv(
//            @Query("api_key") String apiKey,
//            @Query("language") String language,
//            @Query("query") String query
//    );
//
//    @GET("discover/movie")
//    Call<MoviesResponseModel> getReleaseMovies(
//            @Query("api_key") String apiKey,
//            @Query("primary_release_date.gte") String gte,
//            @Query("primary_release_date.lte") String lte
//    );

}
