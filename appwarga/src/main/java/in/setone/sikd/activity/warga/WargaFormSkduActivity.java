package in.setone.sikd.activity.warga;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import in.setone.sikd.R;
import in.setone.sikd.model.warga.requestv2.Response;
import in.setone.sikd.netwok.warga.WargaRetrofitService;
import in.setone.sikd.netwok.warga.WargaServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

public class WargaFormSkduActivity extends AppCompatActivity {

    WargaServicesAPI servicesAPI;
    EditText kep, nama, alamat;
    Button sendRequest;
    int surat;

    private TextView tvback;
    private ImageView icback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_skudv2_warga);
        icback = (ImageView) findViewById(R.id.icBack);
        tvback = (TextView) findViewById(R.id.tvBack);

        servicesAPI = WargaRetrofitService.createServiceDefault(WargaServicesAPI.class);

        sendRequest = (Button) findViewById(R.id.btnSendSkdu);
        kep = (EditText) findViewById(R.id.ed_skdu_kep);
        nama = (EditText) findViewById(R.id.ed_skdu_namaperusahaan);
        alamat = (EditText) findViewById(R.id.ed_skdu_alamatperusahaan);

        tvback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        icback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        final Intent intent = getIntent();
        surat = intent.getIntExtra("surat",0);

        sendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hsv = nama.getText().toString()+'#'+alamat.getText().toString();
                Log.d("CEKK", hsv);
                if (!hsv.isEmpty()){
                    servicesAPI.requestv2(WargaPreferences.getLoggedInNIK(getBaseContext()), surat, kep.getText().toString(), hsv).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            if (response.body().isStatus()){
                                Toast.makeText(getApplicationContext(), "Surat berhasil di kirim !!!", Toast.LENGTH_SHORT).show();
                                Intent intent1 = new Intent(WargaFormSkduActivity.this, WargaMainActivity.class);
                                startActivity(intent1);
                                finish();
                            }else {
                                Toast.makeText(getApplicationContext(), "Surat gagal di kirim !!!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {

                        }
                    });
                }else {
                    Toast.makeText(WargaFormSkduActivity.this, "Tidak boleh ada yang Kosong !!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
