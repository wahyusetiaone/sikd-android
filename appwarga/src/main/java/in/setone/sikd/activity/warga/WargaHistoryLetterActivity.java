package in.setone.sikd.activity.warga;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import in.setone.sikd.R;
import in.setone.sikd.adapter.warga.LetterHistoryAdapter;
import in.setone.sikd.model.warga.history.Response;
import in.setone.sikd.model.warga.history.Result;
import in.setone.sikd.netwok.warga.WargaRetrofitService;
import in.setone.sikd.netwok.warga.WargaServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

import java.util.ArrayList;
import java.util.List;

public class WargaHistoryLetterActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public static String KEY_NO = "nosurat";
    public static String KEY_SUART = "surat";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private WargaServicesAPI servicesAPI;
    private RecyclerView recyclerView;
    private List<Result> data = new ArrayList<>();
    private LetterHistoryAdapter adapter;
    private TextView nothing;
    private TextView tvback;
    private ImageView icback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_letterv2_warga);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container_history);
        nothing = (TextView) findViewById(R.id.tvNothing);

        icback = (ImageView) findViewById(R.id.icBack);
        tvback = (TextView) findViewById(R.id.tvBack);

        recyclerView = (RecyclerView) findViewById(R.id.rcLetterHistory);

        adapter = new LetterHistoryAdapter(data, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(WargaHistoryLetterActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });

        tvback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        icback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

    }

    private void loadRecyclerViewData() {

        mSwipeRefreshLayout.setRefreshing(true);
        servicesAPI = WargaRetrofitService.createServiceDefault(WargaServicesAPI.class);

        servicesAPI.history(WargaPreferences.getLoggedInNIK(this)).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.body().isStatus()){
                    data.clear();
                    data.addAll(response.body().getResult());
                    adapter.notifyDataSetChanged();
                    nothing.setVisibility(View.GONE);
                }else {
                    nothing.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), "Tidak ada riwayat surat !!!",Toast.LENGTH_SHORT).show();
                }

                // Stopping swipe refresh
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                nothing.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Tidak ada riwayat surat !!!",Toast.LENGTH_SHORT).show();


                // Stopping swipe refresh
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }
}
