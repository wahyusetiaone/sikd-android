package in.setone.sikd.activity.warga;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import in.setone.sikd.R;
import in.setone.sikd.model.warga.requestv2.Response;
import in.setone.sikd.netwok.warga.WargaRetrofitService;
import in.setone.sikd.netwok.warga.WargaServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

public class WargaFormSklActivity extends AppCompatActivity {

    WargaServicesAPI servicesAPI;
    EditText kep, kepala, kk, nama, jenis_kelamin, tempat_kelahiran, tempat_dlahirkan, hari,
            pukul, jenis_kelahiran, penolong, berat, panjang, ibu, ayah, saksiI, saksiII;
    Button sendRequest;
    int surat;

    private TextView tvback;
    private ImageView icback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_sklv2_warga);
        icback = (ImageView) findViewById(R.id.icBack);
        tvback = (TextView) findViewById(R.id.tvBack);

        servicesAPI = WargaRetrofitService.createServiceDefault(WargaServicesAPI.class);

        sendRequest = (Button) findViewById(R.id.btnSendSkl);
        kep = (EditText) findViewById(R.id.ed_skl_kep);
        kepala = (EditText) findViewById(R.id.ed_skl_kepala);
        kk = (EditText) findViewById(R.id.ed_skl_kk);
        nama = (EditText) findViewById(R.id.ed_skl_nama);
        jenis_kelamin = (EditText) findViewById(R.id.ed_skl_jenis_kelamin);
        tempat_kelahiran = (EditText) findViewById(R.id.ed_skl_tempat_kelahirkan);
        tempat_dlahirkan = (EditText) findViewById(R.id.ed_skl_tempat_dlahirkan);
        hari = (EditText) findViewById(R.id.ed_skl_hari);
        pukul = (EditText) findViewById(R.id.ed_skl_pukul);
        jenis_kelahiran = (EditText) findViewById(R.id.ed_skl_jenis_kelahiran);
        penolong = (EditText) findViewById(R.id.ed_skl_penolong);
        berat = (EditText) findViewById(R.id.ed_skl_berat);
        panjang = (EditText) findViewById(R.id.ed_skl_panjang);
        ibu = (EditText) findViewById(R.id.ed_skl_ibu);
        ayah = (EditText) findViewById(R.id.ed_skl_ayah);
        saksiI = (EditText) findViewById(R.id.ed_skl_saksiI);
        saksiII = (EditText) findViewById(R.id.ed_skl_saksiII);

        tvback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        icback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        final Intent intent = getIntent();
        surat = intent.getIntExtra("surat",0);

        sendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hsv = kepala.getText().toString()+'#'+kk.getText().toString()+'#'+nama.getText().toString()+
                        '#'+jenis_kelamin.getText().toString()+'#'+tempat_kelahiran.getText().toString()+'#'+tempat_dlahirkan.getText().toString()+
                        '#'+hari.getText().toString()+'#'+pukul.getText().toString()+'#'+jenis_kelahiran.getText().toString()+
                        '#'+penolong.getText().toString()+'#'+berat.getText().toString()+'#'+panjang.getText().toString()+
                        '#'+ibu.getText().toString()+'#'+ayah.getText().toString()+'#'+saksiI.getText().toString()+
                        '#'+saksiII.getText().toString();
                Log.d("CEKK", hsv);
                if (!hsv.isEmpty()){
                    servicesAPI.requestv2(WargaPreferences.getLoggedInNIK(getBaseContext()), surat, kep.getText().toString(), hsv).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            if (response.body().isStatus()){
                                Toast.makeText(getApplicationContext(), "Surat berhasil di kirim !!!", Toast.LENGTH_SHORT).show();
                                Intent intent1 = new Intent(WargaFormSklActivity.this, WargaMainActivity.class);
                                startActivity(intent1);
                                finish();
                            }else {
                                Toast.makeText(getApplicationContext(), "Surat gagal di kirim !!!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {

                        }
                    });
                }else {
                    Toast.makeText(WargaFormSklActivity.this, "Tidak boleh ada yang Kosong !!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
