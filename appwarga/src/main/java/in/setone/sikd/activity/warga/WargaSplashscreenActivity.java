package in.setone.sikd.activity.warga;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import in.setone.sikd.R;

public class WargaSplashscreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen_warga);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(WargaSplashscreenActivity.this, WargaLoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }
}
