package in.setone.sikd.activity.warga;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import in.setone.sikd.R;
import in.setone.sikd.model.warga.user.Response;
import in.setone.sikd.netwok.warga.WargaRetrofitService;
import in.setone.sikd.netwok.warga.WargaServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

import java.util.ArrayList;
import java.util.List;

public class WargaMainActivity extends AppCompatActivity {

    private TextView name, tempattgllhr, jeniskelamin, agama, alamat, pekerjaan;
    private Button btnRiwayat;
    private LinearLayout cA,cB,cC,cD, cCC, cDD;
    private ImageView btnLogout;
    private WargaServicesAPI servicesAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainv2_warga);

        name = (TextView) findViewById(R.id.tvName);
        btnRiwayat = (Button) findViewById(R.id.tvRiwayat);
        cA = (LinearLayout) findViewById(R.id.cA);
        cB = (LinearLayout) findViewById(R.id.cB);
        cC = (LinearLayout) findViewById(R.id.cC);
        cD = (LinearLayout) findViewById(R.id.cD);
        cCC = (LinearLayout) findViewById(R.id.cCC);
        cDD = (LinearLayout) findViewById(R.id.cDD);
        btnLogout = (ImageView) findViewById(R.id.btnLogout);

        tempattgllhr = (TextView) findViewById(R.id.tvotgllahir);
        jeniskelamin = (TextView) findViewById(R.id.tvojeniskelamin);
        agama = (TextView) findViewById(R.id.tvoagama);
        alamat = (TextView) findViewById(R.id.tvoalamat);
        pekerjaan = (TextView) findViewById(R.id.tvopekerjaan);

        name.setText(WargaPreferences.getRegisteredUser(this));
        servicesAPI = WargaRetrofitService.createServiceDefault(WargaServicesAPI.class);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WargaPreferences.clearLoggedInUser(getBaseContext());
                Intent intent = new Intent(WargaMainActivity.this, WargaLoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnRiwayat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WargaMainActivity.this, WargaHistoryLetterActivity.class);
                startActivity(intent);
            }
        });

        cA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WargaMainActivity.this, WargaFormSktmActivity.class);
                intent.putExtra("surat", 9001);
                startActivity(intent);
            }
        });

        cB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WargaMainActivity.this, WargaFormSihActivity.class);
                intent.putExtra("surat", 9002);
                startActivity(intent);
            }
        });

        cC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WargaMainActivity.this, WargaFormSkkActivity.class);
                intent.putExtra("surat", 9003);
                startActivity(intent);
            }
        });

        cD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WargaMainActivity.this, WargaFormSkduActivity.class);
                intent.putExtra("surat", 9004);
                startActivity(intent);
            }
        });

        cCC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WargaMainActivity.this, WargaFormSklActivity.class);
                intent.putExtra("surat", 9005);
                startActivity(intent);
            }
        });

        cDD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WargaMainActivity.this, WargaFormSkktpActivity.class);
                intent.putExtra("surat", 9006);
                startActivity(intent);
            }
        });

        servicesAPI.userInformation(WargaPreferences.getLoggedInNIK(this)).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                List<in.setone.sikd.model.warga.user.Result> data = new ArrayList<>();
                data.addAll(response.body().getResult());
                if (data.size() == 1){
                    tempattgllhr.setText(data.get(0).getPlaceofbirth()+","+data.get(0).getBirthday());
                    jeniskelamin.setText(data.get(0).getGender());
                    agama.setText(data.get(0).getReligion());
                    alamat.setText(data.get(0).getAddress());
                    pekerjaan.setText(data.get(0).getJobstatus());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
    }
}
