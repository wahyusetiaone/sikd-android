package in.setone.sikd.activity.warga;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import in.setone.sikd.R;
import in.setone.sikd.model.warga.login.Response;
import in.setone.sikd.model.warga.login.Result;
import in.setone.sikd.netwok.warga.WargaRetrofitService;
import in.setone.sikd.netwok.warga.WargaServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

import java.util.ArrayList;
import java.util.List;

public class WargaLoginActivity extends AppCompatActivity {

    private WargaServicesAPI servicesAPI;
    private EditText inputKK, inputNIK;
    private Button btnLogin;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_warga);

        inputKK = (EditText) findViewById(R.id.inputKK);
        inputNIK = (EditText) findViewById(R.id.inputNIK);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        servicesAPI = WargaRetrofitService.createServiceDefault(WargaServicesAPI.class);

        intent = new Intent(this, WargaMainActivity.class);

        if (WargaPreferences.getLoggedInStatus(getBaseContext())){
            startActivity(intent);
            finish();
        }else {
            regisUser();
        }
    }

    private void regisUser(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicesAPI.login(inputKK.getText().toString(),inputNIK.getText().toString()).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        if (response.body().isStatus()){
                            Log.d("LoginActivity", response.body().getResult().toString());
                            List<Result> result = response.body().getResult();
                            String nik = result.get(0).getNik();
                            if (result.size() == 1){
                                servicesAPI.userInformation(nik).enqueue(new Callback<in.setone.sikd.model.warga.user.Response>() {
                                    @Override
                                    public void onResponse(Call<in.setone.sikd.model.warga.user.Response> calle, retrofit2.Response<in.setone.sikd.model.warga.user.Response> responsee) {
                                        List<in.setone.sikd.model.warga.user.Result> data = new ArrayList<>();
                                        data.addAll(responsee.body().getResult());
                                        Log.d("Login Activity", responsee.body().getMessage());
                                        Log.d("Login Activity", data.toString());
                                        if (data.size() == 1){
                                            WargaPreferences.setLoggedInStatus(getBaseContext(),true);
                                            WargaPreferences.setRegisteredUser(getBaseContext(),data.get(0).getName());
                                            WargaPreferences.setLoggedInKK(getBaseContext(),data.get(0).getKk());
                                            WargaPreferences.setLoggedInNIK(getBaseContext(),data.get(0).getNik());
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            getApplication().startActivity(intent);
                                            finish();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<in.setone.sikd.model.warga.user.Response> calle, Throwable te) {

                                    }
                                });
                            }
                        }else {
                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Gagal terhubung Jaringan !!!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
