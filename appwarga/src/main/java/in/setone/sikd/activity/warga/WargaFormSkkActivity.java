package in.setone.sikd.activity.warga;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import in.setone.sikd.R;
import in.setone.sikd.model.warga.requestv2.Response;
import in.setone.sikd.netwok.warga.WargaRetrofitService;
import in.setone.sikd.netwok.warga.WargaServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

public class WargaFormSkkActivity extends AppCompatActivity {

    WargaServicesAPI servicesAPI;
    EditText kep, tgl, pukul, penyebab, tempat, menerangkan, ayah, ibu, pelapor, saksiI, saksiII;
    Button sendRequest;
    int surat;

    private TextView tvback;
    private ImageView icback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_skkv2_warga);
        icback = (ImageView) findViewById(R.id.icBack);
        tvback = (TextView) findViewById(R.id.tvBack);

        servicesAPI = WargaRetrofitService.createServiceDefault(WargaServicesAPI.class);

        sendRequest = (Button) findViewById(R.id.btnSendSkk);
        kep = (EditText) findViewById(R.id.ed_skk_kep);
        tgl = (EditText) findViewById(R.id.ed_skk_tgl);
        pukul = (EditText) findViewById(R.id.ed_skk_pukul);
        penyebab = (EditText) findViewById(R.id.ed_skk_penyebab);
        tempat = (EditText) findViewById(R.id.ed_skk_tempat);
        menerangkan = (EditText) findViewById(R.id.ed_skk_menerangkan);
        ayah = (EditText) findViewById(R.id.ed_skk_ayah);
        ibu = (EditText) findViewById(R.id.ed_skk_ibu);
        pelapor = (EditText) findViewById(R.id.ed_skk_pelapor);
        saksiI = (EditText) findViewById(R.id.ed_skk_saksiI);
        saksiII = (EditText) findViewById(R.id.ed_skk_saksiII);

        tvback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        icback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        final Intent intent = getIntent();
        surat = intent.getIntExtra("surat",0);

        sendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String hsv = tgl.getText().toString()+'#'+pukul.getText().toString()+'#'+penyebab.getText().toString()+
                        '#'+tempat.getText().toString()+'#'+menerangkan.getText().toString()+'#'+ayah.getText().toString()+
                        '#'+ibu.getText().toString()+'#'+pelapor.getText().toString()+'#'+saksiI.getText().toString()+'#'+saksiII.getText().toString();
                Log.d("CEKK", hsv);
                if (!hsv.isEmpty()){
                    servicesAPI.requestv2(WargaPreferences.getLoggedInNIK(getBaseContext()), surat, kep.getText().toString(), hsv).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            if (response.body().isStatus()){
                                Toast.makeText(getApplicationContext(), "Surat berhasil di kirim !!!", Toast.LENGTH_SHORT).show();
                                Intent intent1 = new Intent(WargaFormSkkActivity.this, WargaMainActivity.class);
                                startActivity(intent1);
                                finish();
                            }else {
                                Toast.makeText(getApplicationContext(), "Surat gagal di kirim !!!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {

                        }
                    });
                }else {
                    Toast.makeText(WargaFormSkkActivity.this, "Tidak boleh ada yang Kosong !!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
