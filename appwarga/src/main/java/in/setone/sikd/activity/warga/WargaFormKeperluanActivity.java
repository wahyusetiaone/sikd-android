package in.setone.sikd.activity.warga;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import in.setone.sikd.R;
import in.setone.sikd.model.warga.request.Response;
import in.setone.sikd.netwok.warga.WargaRetrofitService;
import in.setone.sikd.netwok.warga.WargaServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

public class WargaFormKeperluanActivity extends AppCompatActivity {

    private EditText keperluan;
    private Button btnMinta;
    private int surat;
    private WargaServicesAPI servicesAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_keperluan);

        servicesAPI = WargaRetrofitService.createServiceDefault(WargaServicesAPI.class);

        btnMinta = (Button) findViewById(R.id.btnMintaSurat);
        keperluan = (EditText) findViewById(R.id.inputKeperluan);

        final Intent intent = getIntent();
        surat = intent.getIntExtra("surat",0);

        btnMinta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicesAPI.request(WargaPreferences.getLoggedInNIK(getBaseContext()), surat, keperluan.getText().toString()).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        if (response.body().isStatus()){
                            Toast.makeText(getApplicationContext(), "Surat berhasil di kirim !!!", Toast.LENGTH_SHORT).show();
                            Intent intent1 = new Intent(WargaFormKeperluanActivity.this, WargaMainActivity.class);
                            startActivity(intent1);
                            finish();
                        }else {
                            Toast.makeText(getApplicationContext(), "Surat gagal di kirim !!!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {

                    }
                });
            }
        });
    }
}
