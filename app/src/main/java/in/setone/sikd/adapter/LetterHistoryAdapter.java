package in.setone.sikd.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.sikd.DownloadLetterActivity;
import in.setone.sikd.R;
import in.setone.sikd.model.warga.history.Result;

import java.util.List;

public class LetterHistoryAdapter extends RecyclerView.Adapter<LetterHistoryAdapter.LetterViewHolder> {


    private List<Result> data;
    private Activity activity;

    public LetterHistoryAdapter(List<Result> data, Activity activity) {
        this.data = data;
        this.activity = activity;
    }

    @NonNull
    @Override
    public LetterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_list_letter_history, parent, false);
        return new LetterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LetterViewHolder holder, int position) {
        final Result item = data.get(position);
        holder.name.setText(item.getName());
        holder.surat.setText(item.getNama_surat());

        String st = "";
        int color = 0;
        switch (item.getStatus()) {
            case "1":
                st = "Diterbitkan";
                color = R.color.pink;
                break;
            case "R":
                st = "Menunggu persetujuan RT";
                color = R.color.colorAccent;
                break;
            case "R0":
                st = "Ditolak RT";
                color = R.color.coklattua;
                break;
            case "W0":
                st = "Ditolak RW";
                color = R.color.coklattua;
                break;
            case "W":
                st = "Menunggu persetujuan RW";
                color = R.color.colorAccent;
                break;
            case "K0":
                st = "Ditolak kelurahan";
                color = R.color.coklattua;
                break;
            case "K":
                st = "Menunggu persetujuan Kelurahan";
                color = R.color.colorAccent;
                break;
        }
        final String stu = st;
        holder.status.setText(st);
        holder.status.setTextColor(color);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.getStatus().equals("1")){
                    Intent intent = new Intent(holder.itemView.getContext(), DownloadLetterActivity.class);
                    intent.putExtra("_id_transaksi", String.valueOf(item.get_id_transaksi()));
                    intent.putExtra("kk", String.valueOf(item.getKk()));
                    intent.putExtra("nik", String.valueOf(item.getNik()));
                    intent.putExtra("nama", String.valueOf(item.getName()));
                    intent.putExtra("alamat", String.valueOf(item.getAddress()));
                    intent.putExtra("tempatL", "Karanganyar");
                    intent.putExtra("tanggalL", String.valueOf(item.getBirthday()));
                    intent.putExtra("jenis_kelamin", String.valueOf(item.getGender()));
                    intent.putExtra("jenis_surat", String.valueOf(item.getNama_surat()));
                    intent.putExtra("keperluan", String.valueOf(item.getKeperluan()));

                    holder.itemView.getContext().startActivity(intent);
                }else {
                    Toast.makeText(holder.itemView.getContext(), "Status surat "+stu,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (data != null) ? data.size() : 0;
    }

    public class LetterViewHolder extends RecyclerView.ViewHolder {
        private TextView name, surat, status;

        public LetterViewHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tvNameLetterHistory);
            surat = (TextView) itemView.findViewById(R.id.tvJenisLetterHistory);
            status = (TextView) itemView.findViewById(R.id.tvStatusHistory);
        }
    }
}
