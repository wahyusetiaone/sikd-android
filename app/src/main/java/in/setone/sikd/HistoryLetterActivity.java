package in.setone.sikd;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.sikd.adapter.LetterHistoryAdapter;
import in.setone.sikd.model.warga.history.Response;
import in.setone.sikd.model.warga.history.Result;
import in.setone.sikd.netwok.RetrofitService;
import in.setone.sikd.netwok.ServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

import java.util.ArrayList;
import java.util.List;

public class HistoryLetterActivity extends AppCompatActivity {

    public static String KEY_NO = "nosurat";
    public static String KEY_SUART = "surat";

    private ServicesAPI servicesAPI;
    private RecyclerView recyclerView;
    private List<Result> data = new ArrayList<>();
    private LetterHistoryAdapter adapter;
    private TextView nothing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_letter);

        nothing = (TextView) findViewById(R.id.tvNothing);

        recyclerView = (RecyclerView) findViewById(R.id.rcLetterHistory);

        adapter = new LetterHistoryAdapter(data, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HistoryLetterActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        servicesAPI = RetrofitService.createServiceDefault(ServicesAPI.class);

        servicesAPI.history(Preferences.getLoggedInNIK(this)).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.body().isStatus()){
                    data.addAll(response.body().getResult());
                    adapter.notifyDataSetChanged();
                    nothing.setVisibility(View.GONE);
                }else {
                    nothing.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), "Tidak ada riwayat surat !!!",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                nothing.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Tidak ada riwayat surat !!!",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
