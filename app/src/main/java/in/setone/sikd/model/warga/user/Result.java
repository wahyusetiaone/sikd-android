package in.setone.sikd.model.warga.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.sql.Date;

public class Result implements Serializable {


    @SerializedName("_id")
    @Expose
    private int _id;

    @SerializedName("nik")
    @Expose
    private String nik;

    @SerializedName("kk")
    @Expose
    private String kk;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("birthday")
    @Expose
    private Date birthday;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("rt")
    @Expose
    private int rt;

    @SerializedName("gender")
    @Expose
    private String gender;

    public Result(int _id, String nik, String kk, String name, Date birthday, String address, int rt, String gender) {
        this._id = _id;
        this.nik = nik;
        this.kk = kk;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.rt = rt;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Result{" +
                "_id=" + _id +
                ", nik='" + nik + '\'' +
                ", kk='" + kk + '\'' +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", address='" + address + '\'' +
                ", rt=" + rt +
                ", gender='" + gender + '\'' +
                '}';
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getKk() {
        return kk;
    }

    public void setKk(String kk) {
        this.kk = kk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRt() {
        return rt;
    }

    public void setRt(int rt) {
        this.rt = rt;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
