package in.setone.sikd.model.warga.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("kk")
    @Expose
    private String kk;

    @SerializedName("nik")
    @Expose
    private String nik;

    public Result(String kk, String nik) {
        this.kk = kk;
        this.nik = nik;
    }

    @Override
    public String toString() {
        return "Result{" +
                "kk='" + kk + '\'' +
                ", nik='" + nik + '\'' +
                '}';
    }

    public String getKk() {
        return kk;
    }

    public void setKk(String kk) {
        this.kk = kk;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }
}
