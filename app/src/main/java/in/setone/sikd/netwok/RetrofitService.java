package in.setone.sikd.netwok;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import in.setone.sikd.BuildConfig;
import in.setone.sikd.R;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import java.io.IOException;

public class RetrofitService {

    private static volatile String API_TOKEN;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(serviceClass);
    }


    /**This only to request login and register*/
    public static <S> S createServiceDefault(Class<S> serviceClass) {
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request request = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .build();

                return chain.proceed(request);
            }
        };
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(interceptor);
        OkHttpClient client = httpClient.build();

        Gson gSon=  new GsonBuilder().setDateFormat("yyyy-MM-dd").create();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.URL_API)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gSon))
                .build();
        return retrofit.create(serviceClass);
    }

}
