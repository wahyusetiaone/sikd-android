package in.setone.sikd;

import android.content.Context;
import android.content.Intent;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import in.setone.sikd.adapter.LetterHistoryAdapter;

public class DownloadLetterActivity extends AppCompatActivity {

    private WebView webView;
    private Button btnUnduh;
    private String id,surat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_letter);

        try {

            Intent intent = getIntent();
            id = intent.getStringExtra("_id_transaksi");
            surat = intent.getStringExtra("jenis_surat");
            Log.d("DownloadLetterActivity", surat + " " + String.valueOf(id));

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("getStringExtra_EX", e + "");
        }

        webView = (WebView) findViewById(R.id.webView);
        btnUnduh = (Button) findViewById(R.id.btnUnduh);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                btnUnduh.setVisibility(View.VISIBLE);
                //if page loaded successfully then show print button
//                findViewById(R.id.fab).setVisibility(View.VISIBLE);
            }
        });

        switch (surat){
            case "Surat Izin Jamuan":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_jamuan/"+ id);
                break;

            case "Surat Keterangan Tidak Mampu":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_sktm/"+ id);
                break;

            case "Surat Keterangan Kehilangan":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_kehilangan/"+ id);
                break;

            case "Surat Keterangan Kematian":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_kematian/"+ id);
                break;

        }

        btnUnduh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printPDF(view);
            }
        });

    }

    //create a function to create the print job
    private void createWebPrintJob(WebView webView) {

        //create object of print manager in your device
        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

        //create object of print adapter
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

        //provide name to your newly generated pdf file
        String jobName = getString(R.string.app_name) + " Print Test";

        //open print dialog
        printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());
    }

    //perform click pdf creation operation on click of print button click
    public void printPDF(View view) {
        createWebPrintJob(webView);
    }
}
