package in.setone.sikd;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    static final String KEY_USER_TEREGISTER ="user";
    static final String KEY_KK_SEDANG_LOGIN ="KK_logged_in";
    static final String KEY_NIK_SEDANG_LOGIN = "NIK_logged_in";
    static final String KEY_STATUS_SEDANG_LOGIN = "Status_logged_in";

    private static SharedPreferences getSharedPreference(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setRegisteredUser(Context context, String name){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_TEREGISTER, name);
        editor.apply();
    }
    public static String getRegisteredUser(Context context){
        return getSharedPreference(context).getString(KEY_USER_TEREGISTER,"");
    }

    public static void setLoggedInKK(Context context, String rt){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_KK_SEDANG_LOGIN, rt);
        editor.apply();
    }
    public static String getLoggedInKK(Context context){
        return getSharedPreference(context).getString(KEY_KK_SEDANG_LOGIN,"0");
    }

    public static void setLoggedInNIK(Context context, String nip){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_NIK_SEDANG_LOGIN, nip);
        editor.apply();
    }
    public static String getLoggedInNIK(Context context){
        return getSharedPreference(context).getString(KEY_NIK_SEDANG_LOGIN,"");
    }

    public static void setLoggedInStatus(Context context, boolean status){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(KEY_STATUS_SEDANG_LOGIN,status);
        editor.apply();
    }
    public static boolean getLoggedInStatus(Context context){
        return getSharedPreference(context).getBoolean(KEY_STATUS_SEDANG_LOGIN,false);
    }

    public static void clearLoggedInUser (Context context){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(KEY_USER_TEREGISTER);
        editor.remove(KEY_KK_SEDANG_LOGIN);
        editor.remove(KEY_NIK_SEDANG_LOGIN);
        editor.remove(KEY_STATUS_SEDANG_LOGIN);
        editor.apply();
    }
}