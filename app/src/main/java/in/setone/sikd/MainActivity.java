package in.setone.sikd;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;

public class MainActivity extends AppCompatActivity {

    private TextView name, btnRiwayat;
    private CardView cA,cB,cC,cD;
    private ImageView btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (TextView) findViewById(R.id.tvName);
        btnRiwayat = (TextView) findViewById(R.id.tvRiwayat);
        cA = (CardView) findViewById(R.id.cA);
        cB = (CardView) findViewById(R.id.cB);
        cC = (CardView) findViewById(R.id.cC);
        cD = (CardView) findViewById(R.id.cD);
        btnLogout = (ImageView) findViewById(R.id.btnLogout);

        name.setText(Preferences.getRegisteredUser(this));

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.clearLoggedInUser(getBaseContext());
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnRiwayat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HistoryLetterActivity.class);
                startActivity(intent);
            }
        });

        cA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FormKeperluanActivity.class);
                intent.putExtra("surat", 9001);
                startActivity(intent);
            }
        });

        cB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FormKeperluanActivity.class);
                intent.putExtra("surat", 9002);
                startActivity(intent);
            }
        });

        cC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FormKeperluanActivity.class);
                intent.putExtra("surat", 9003);
                startActivity(intent);
            }
        });

        cD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FormKeperluanActivity.class);
                intent.putExtra("surat", 9004);
                startActivity(intent);
            }
        });
    }
}
