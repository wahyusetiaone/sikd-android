package in.setone.apprw.activity.rw;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import in.setone.apprw.R;
import in.setone.apprw.adapter.rw.LetterAdapter;
import in.setone.apprw.model.rw.history.Response;
import in.setone.apprw.model.rw.history.Result;
import in.setone.apprw.netwok.rw.RetrofitService;
import in.setone.apprw.netwok.rw.ServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ServicesAPI servicesAPI;
    private RecyclerView recyclerView;
    private List<Result> data = new ArrayList<>();
    private LetterAdapter adapter;
    private TextView antrian, name, history;
    private ImageView logout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainv2_rw);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        name = (TextView) findViewById(R.id.tvName);
        antrian = (TextView) findViewById(R.id.tvAntrian);
        history = (TextView) findViewById(R.id.historyLetter);
        logout = (ImageView) findViewById(R.id.logout);

        recyclerView = (RecyclerView) findViewById(R.id.rcLetter);

        history.bringToFront();

        mSwipeRefreshLayout.setOnRefreshListener(this);

        adapter = new LetterAdapter(data);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HistoryActivity.class);
                startActivity(intent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.clearLoggedInUser(getBaseContext());
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        name.setText(Preferences.getRegisteredUser(this));
        Log.d("MainActivity", String.valueOf(Preferences.getLoggedInRt(this)));

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });
    }

    private void loadRecyclerViewData() {
        // Showing refresh animation before making http call
        mSwipeRefreshLayout.setRefreshing(true);
        servicesAPI = RetrofitService.createServiceDefault(ServicesAPI.class);
        servicesAPI.letterHistoryWaiting(Preferences.getLoggedInRt(getBaseContext())).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.body().isStatus()){
                    if (!response.body().getResult().isEmpty()){
                        data.clear();
                        data.addAll(response.body().getResult());
                        Log.d("MainActivity", data.toString());
                        adapter.notifyDataSetChanged();
                        antrian.setVisibility(View.INVISIBLE);
                    }else {
                        antrian.setVisibility(View.VISIBLE);
                    }
                }else {
                    antrian.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

                // Stopping swipe refresh
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.e("MainActivity", t.getMessage());
                Toast.makeText(getApplicationContext(), "Gagal terhubung Server !!!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }
}
