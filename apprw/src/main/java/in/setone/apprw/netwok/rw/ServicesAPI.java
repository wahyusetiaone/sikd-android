package in.setone.apprw.netwok.rw;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicesAPI {

    @FormUrlEncoded
    @POST("login")
    Call<in.setone.apprw.model.rw.login.Response> login(
            @Field("nip") String nip
    );

    @FormUrlEncoded
    @POST("get")
    Call<in.setone.apprw.model.rw.user.Response> userInfomation(
            @Field("nip") String nip
    );

    @FormUrlEncoded
    @POST("history")
    Call<in.setone.apprw.model.rw.history.Response> letterHistory(
            @Field("rw") int rw
    );
    @FormUrlEncoded
    @POST("historyWaiting")
    Call<in.setone.apprw.model.rw.history.Response> letterHistoryWaiting(
            @Field("rw") int rw
    );

    @FormUrlEncoded
    @POST("update")
    Call<in.setone.apprw.model.rw.update.Response> updateStatusLetter(
            @Field("id_transaksi") String id_transaksi,
            @Field("status") String status
    );
}
