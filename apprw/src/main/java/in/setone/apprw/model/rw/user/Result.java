package in.setone.apprw.model.rw.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.sql.Date;

public class Result implements Serializable {


//      "_id": 2,
//              "nip": "12344",
//              "nik": "1234567897654321",
//              "code": "1",
//              "kk": "1234567897654300",
//              "name": "Milea",
//              "birthday": "2020-07-15",
//              "address": "Dangen, 21/21, Karanganyar",
//              "rt": "2",
//              "gender": "Perempuan"

    @SerializedName("_id")
    @Expose
    private int _id;

    @SerializedName("nip")
    @Expose
    private String nip;

    @SerializedName("nik")
    @Expose
    private String nik;

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("kk")
    @Expose
    private String kk;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("birthday")
    @Expose
    private Date brithday;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("rt")
    @Expose
    private int rt;

    @SerializedName("gender")
    @Expose
    private String gender;

    public Result(int _id, String nip, String nik, int code, String kk, String name, Date brithday, String address, int rt, String gender) {
        this._id = _id;
        this.nip = nip;
        this.nik = nik;
        this.code = code;
        this.kk = kk;
        this.name = name;
        this.brithday = brithday;
        this.address = address;
        this.rt = rt;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Result{" +
                "_id=" + _id +
                ", nip='" + nip + '\'' +
                ", nik='" + nik + '\'' +
                ", code=" + code +
                ", kk='" + kk + '\'' +
                ", name='" + name + '\'' +
                ", brithday=" + brithday +
                ", address='" + address + '\'' +
                ", rt=" + rt +
                ", gender='" + gender + '\'' +
                '}';
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getKk() {
        return kk;
    }

    public void setKk(String kk) {
        this.kk = kk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBrithday() {
        return brithday;
    }

    public void setBrithday(Date brithday) {
        this.brithday = brithday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRt() {
        return rt;
    }

    public void setRt(int rt) {
        this.rt = rt;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
