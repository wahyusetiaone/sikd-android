package in.setone.pelayanansurat;

import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import in.setone.pelayanansurat.activity.warga.WargaLoginActivity;
import in.setone.pelayanansurat.activity.warga.WargaMainActivity;
import in.setone.pelayanansurat.activity.warga.WargaPreferences;

public class MainActivity extends AppCompatActivity {

    LinearLayout btnWarga, btnRT, btnRW;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (WargaPreferences.getLoggedInStatus(getBaseContext())){
            Intent intent = new Intent(MainActivity.this, WargaMainActivity.class);
            startActivity(intent);
            finish();
        }else if (in.setone.pelayanansurat.activity.rt.Preferences.getLoggedInStatus(getBaseContext())){
            Intent intent = new Intent(MainActivity.this, in.setone.pelayanansurat.activity.rt.MainActivity.class);
            startActivity(intent);
            finish();
        }else if (in.setone.pelayanansurat.activity.rw.Preferences.getLoggedInStatus(getBaseContext())){
            Intent intent = new Intent(MainActivity.this, in.setone.pelayanansurat.activity.rw.MainActivity.class);
            startActivity(intent);
            finish();
        }

        btnRT = (LinearLayout) findViewById(R.id.btnRt);
        btnWarga = (LinearLayout) findViewById(R.id.btnWarga);
        btnRW = (LinearLayout) findViewById(R.id.btnRw);

        btnRT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, in.setone.pelayanansurat.activity.rt.LoginActivity.class);
                startActivity(i);
            }
        });
        btnWarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, WargaLoginActivity.class);
                startActivity(i);
            }
        });
        btnRW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, in.setone.pelayanansurat.activity.rw.LoginActivity.class);
                startActivity(i);
            }
        });
    }
}
