package in.setone.pelayanansurat.model.rt.history;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.sql.Date;

public class Result implements Serializable {

    //    "_id_transaksi": 10001,
//            "keperluan": "Untuk Melamar Kerja",
//            "status": "R",
//            "kode_surat": 9001,
//            "nama_surat": "Surat Keterangan Tidak Mampu",
//            "format": "021/X/VII/2020",
//            "nik": "1234567891234567",
//            "kk": "1234567891234500",
//            "name": "Abah Setiawan",
//            "birthday": "2020-07-24",
//            "address": "Dangen, 022/21, Karanganyar",
//            "rt": "2",
//            "gender": "Laki-laki"

    @SerializedName("_id_transaksi")
    @Expose
    private int _id_transaksi;

    @SerializedName("keperluan")
    @Expose
    private String keperluan;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("kode_surat")
    @Expose
    private int kode_surat;

    @SerializedName("nama_surat")
    @Expose
    private String nama_surat;

    @SerializedName("format")
    @Expose
    private String format;

    @SerializedName("nik")
    @Expose
    private String nik;

    @SerializedName("kk")
    @Expose
    private String kk;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("birthday")
    @Expose
    private Date birthday;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("rt")
    @Expose
    private int rt;

    @SerializedName("gender")
    @Expose
    private String gender;

    public Result(int _id_transaksi, String keperluan, String status, int kode_surat, String nama_surat, String format, String nik, String kk, String name, Date birthday, String address, int rt, String gender) {
        this._id_transaksi = _id_transaksi;
        this.keperluan = keperluan;
        this.status = status;
        this.kode_surat = kode_surat;
        this.nama_surat = nama_surat;
        this.format = format;
        this.nik = nik;
        this.kk = kk;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.rt = rt;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Result{" +
                "_id_transaksi=" + _id_transaksi +
                ", keperluan='" + keperluan + '\'' +
                ", status='" + status + '\'' +
                ", kode_surat=" + kode_surat +
                ", nama_surat='" + nama_surat + '\'' +
                ", format='" + format + '\'' +
                ", nik='" + nik + '\'' +
                ", kk='" + kk + '\'' +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", address='" + address + '\'' +
                ", rt=" + rt +
                ", gender='" + gender + '\'' +
                '}';
    }

    public int get_id_transaksi() {
        return _id_transaksi;
    }

    public void set_id_transaksi(int _id_transaksi) {
        this._id_transaksi = _id_transaksi;
    }

    public String getKeperluan() {
        return keperluan;
    }

    public void setKeperluan(String keperluan) {
        this.keperluan = keperluan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getKode_surat() {
        return kode_surat;
    }

    public void setKode_surat(int kode_surat) {
        this.kode_surat = kode_surat;
    }

    public String getNama_surat() {
        return nama_surat;
    }

    public void setNama_surat(String nama_surat) {
        this.nama_surat = nama_surat;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getKk() {
        return kk;
    }

    public void setKk(String kk) {
        this.kk = kk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRt() {
        return rt;
    }

    public void setRt(int rt) {
        this.rt = rt;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
