package in.setone.pelayanansurat.model.warga.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.sql.Date;

public class Result implements Serializable {


    @SerializedName("_id")
    @Expose
    private int _id;

    @SerializedName("nik")
    @Expose
    private String nik;

    @SerializedName("kk")
    @Expose
    private String kk;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("birthday")
    @Expose
    private Date birthday;

    @SerializedName("place_of_birth")
    @Expose
    private String placeofbirth;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("religion")
    @Expose
    private String religion;

    @SerializedName("rt")
    @Expose
    private int rt;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("job_status")
    @Expose
    private String jobstatus;

    public Result(int _id, String nik, String kk, String name, Date birthday, String placeofbirth, String address, String religion, int rt, String gender, String jobstatus) {
        this._id = _id;
        this.nik = nik;
        this.kk = kk;
        this.name = name;
        this.birthday = birthday;
        this.placeofbirth = placeofbirth;
        this.address = address;
        this.religion = religion;
        this.rt = rt;
        this.gender = gender;
        this.jobstatus = jobstatus;
    }

    @Override
    public String toString() {
        return "Result{" +
                "_id=" + _id +
                ", nik='" + nik + '\'' +
                ", kk='" + kk + '\'' +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", placeofbirth='" + placeofbirth + '\'' +
                ", address='" + address + '\'' +
                ", religion='" + religion + '\'' +
                ", rt=" + rt +
                ", gender='" + gender + '\'' +
                ", jobstatus='" + jobstatus + '\'' +
                '}';
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getKk() {
        return kk;
    }

    public void setKk(String kk) {
        this.kk = kk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPlaceofbirth() {
        return placeofbirth;
    }

    public void setPlaceofbirth(String placeofbirth) {
        this.placeofbirth = placeofbirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public int getRt() {
        return rt;
    }

    public void setRt(int rt) {
        this.rt = rt;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getJobstatus() {
        return jobstatus;
    }

    public void setJobstatus(String jobstatus) {
        this.jobstatus = jobstatus;
    }
}
