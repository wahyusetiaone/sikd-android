package in.setone.pelayanansurat.model.rt.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("nip")
    @Expose
    private String nip;

    public Result(String nip) {
        this.nip = nip;
    }

    @Override
    public String toString() {
        return "Result{" +
                "nip='" + nip + '\'' +
                '}';
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }
}
