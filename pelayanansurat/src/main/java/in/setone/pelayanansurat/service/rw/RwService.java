package in.setone.pelayanansurat.service.rw;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import in.setone.pelayanansurat.BuildConfig;
import in.setone.pelayanansurat.R;
import in.setone.pelayanansurat.activity.rw.Preferences;
import in.setone.pelayanansurat.activity.rw.HistoryActivity;

import java.util.Arrays;
import java.util.List;

public class RwService extends Service {
    Pusher pusher;
    public RwService() {
        PusherOptions options = new PusherOptions();
        options.setCluster("ap1");

        pusher = new Pusher(BuildConfig.API_KEY_PUSHER, options);

        pusher.connect(new ConnectionEventListener() {
            @Override
            public void onConnectionStateChange(ConnectionStateChange change) {
                Log.i("Pusher", "State changed from " + change.getPreviousState() +
                        " to " + change.getCurrentState());
            }

            @Override
            public void onError(String message, String code, Exception e) {
                Log.i("Pusher", "There was a problem connecting! " +
                        "\ncode: " + code +
                        "\nmessage: " + message +
                        "\nException: " + e
                );
            }
        }, ConnectionState.ALL);

        Channel channel = pusher.subscribe("channel-rw");

        channel.bind("rw-event", new SubscriptionEventListener() {
            @Override
            public void onEvent(PusherEvent event) {
                Log.i("Pusher", "Received event with data: " + event.getData().toString());
                String string = event.getData();
                string = string.substring( 1, string.length() - 1 );
                //[rw,nik,nama,kode_surat,nama_surat,id_transaksi]
                List<String> dataList = Arrays.asList(string.split(","));
                if (dataList.get(0).equals(String.valueOf(Preferences.getLoggedInRw(getApplicationContext())))){
                    Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(getApplicationContext())
                                    .setSmallIcon(R.drawable.ic_email)
                                    .setChannelId(dataList.get(5))
                                    .setSound(alarmSound)
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_suratku))
                                    .setContentTitle("Pengajuan Surat Baru")
                                    .setContentText(dataList.get(2)+" mengajukan "+dataList.get(4)+".");

                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    Intent notificationIntent = new Intent(getApplicationContext(), HistoryActivity.class);

                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0,
                            notificationIntent, 0);

                    mBuilder.setContentIntent(intent);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        NotificationChannel mChannel = new NotificationChannel(dataList.get(5), dataList.get(3), importance);
                        mNotificationManager.createNotificationChannel(mChannel);
                    }
                    mNotificationManager.notify(Integer.parseInt(dataList.get(5)), mBuilder.build());
                }
            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "Service RT Started");
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Service", "Service RT Stoped");
    }
}
