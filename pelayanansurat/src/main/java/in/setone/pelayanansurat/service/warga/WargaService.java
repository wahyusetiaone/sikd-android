package in.setone.pelayanansurat.service.warga;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.PusherEvent;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import in.setone.pelayanansurat.BuildConfig;
import in.setone.pelayanansurat.R;
import in.setone.pelayanansurat.activity.rt.HistoryActivity;
import in.setone.pelayanansurat.activity.rt.Preferences;
import in.setone.pelayanansurat.activity.warga.WargaHistoryLetterActivity;
import in.setone.pelayanansurat.activity.warga.WargaPreferences;

import java.util.Arrays;
import java.util.List;

public class WargaService extends Service {
    Pusher pusher;

    public WargaService() {
        PusherOptions options = new PusherOptions();
        options.setCluster("ap1");

        pusher = new Pusher(BuildConfig.API_KEY_PUSHER, options);

        pusher.connect(new ConnectionEventListener() {
            @Override
            public void onConnectionStateChange(ConnectionStateChange change) {
                Log.i("Pusher", "State changed from " + change.getPreviousState() +
                        " to " + change.getCurrentState());
            }

            @Override
            public void onError(String message, String code, Exception e) {
                Log.i("Pusher", "There was a problem connecting! " +
                        "\ncode: " + code +
                        "\nmessage: " + message +
                        "\nException: " + e
                );
            }
        }, ConnectionState.ALL);

        Channel channel = pusher.subscribe("channel-warga");

        channel.bind("warga-event", new SubscriptionEventListener() {
            @Override
            public void onEvent(PusherEvent event) {
                Log.i("Pusher", "Received event with data: " + event.getData().toString());
                String string = event.getData();
                string = string.substring( 1, string.length() - 1 );
                //[nik,kode_surat,nama_surat,id_transaksi,status]
                List<String> dataList = Arrays.asList(string.split(","));
                if (dataList.get(0).equals(String.valueOf(WargaPreferences.getLoggedInNIK(getApplicationContext())))) {
                    String status = statusTranscode(dataList.get(4));
                    Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(getApplicationContext())
                                    .setSmallIcon(R.drawable.ic_email)
                                    .setChannelId(dataList.get(3))
                                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_suratku))
                                    .setSound(alarmSound)
                                    .setContentTitle("Status Surat "+dataList.get(2))
                                    .setContentText("Pembaruan status surat menjadi "+status+".");

                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    Intent notificationIntent = new Intent(getApplicationContext(), WargaHistoryLetterActivity.class);

                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0,
                            notificationIntent, 0);

                    mBuilder.setContentIntent(intent);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        int importance = NotificationManager.IMPORTANCE_HIGH;
                        NotificationChannel mChannel = new NotificationChannel(dataList.get(3), dataList.get(0), importance);
                        mNotificationManager.createNotificationChannel(mChannel);
                    }
                    mNotificationManager.notify(Integer.parseInt(dataList.get(3)), mBuilder.build());
                }
            }
        });
    }

    private String statusTranscode(String s) {
        switch (s) {
            case "R":
                return "Menunggu Persetujuan RT";
            case "R0":
                return "Ditolak RT";
            case "W":
                return "Menunggu Persetujuan RW";
            case "W0":
                return "Ditolak RW";
            case "K":
                return "Menunggu Persetujuan Kelurahan";
            case "K0":
                return "Ditolak Kelurahan";
            case "1":
                return "Surat dipublikasi";
        }
        return "null";
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
