package in.setone.pelayanansurat.utility;

import android.content.Context;
import android.view.View;
import in.setone.pelayanansurat.R;

public class Helper {

    public static View backgroundLoadChange(View view, Context context, Boolean bool){
        if (bool){
            view.setEnabled(true);
            view.setClickable(true);
            view.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }else {
            view.setEnabled(false);
            view.setClickable(false);
            view.setBackgroundColor(context.getResources().getColor(R.color.coklatmuda));
        }
        return view;
    }
}
