package in.setone.pelayanansurat.activity.rw;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import in.setone.pelayanansurat.R;
import in.setone.pelayanansurat.model.rw.login.Response;
import in.setone.pelayanansurat.model.rw.login.Result;
import in.setone.pelayanansurat.network.rw.RetrofitService;
import in.setone.pelayanansurat.network.rw.ServicesAPI;
import in.setone.pelayanansurat.service.rt.RtService;
import in.setone.pelayanansurat.service.rw.RwService;
import retrofit2.Call;
import retrofit2.Callback;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private ServicesAPI servicesAPI;
    private EditText inputNip;
    private Button btnLogin;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_rw);

        inputNip = (EditText) findViewById(R.id.inputNIP);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        servicesAPI = RetrofitService.createServiceDefault(ServicesAPI.class);

        intent = new Intent(this, MainActivity.class);

        if (Preferences.getLoggedInStatus(getBaseContext())){
            startActivity(intent);
            startOwnService();
            finish();
        }else {
            regisUser();
        }

    }

    private void startOwnService() {
        startService(new Intent(in.setone.pelayanansurat.activity.rw.LoginActivity.this, RwService.class));
    }
    private void regisUser(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLogin.setEnabled(false);
                servicesAPI.login(inputNip.getText().toString()).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        if (response.body().isStatus()){
                            Log.d("LoginActivity", response.body().getResult().toString());
                            List<Result> result = response.body().getResult();
                            if (result.size() == 1){
                                String nip = result.get(0).getNip();
                                servicesAPI.userInfomation(nip).enqueue(new Callback<in.setone.pelayanansurat.model.rw.user.Response>() {
                                    @Override
                                    public void onResponse(Call<in.setone.pelayanansurat.model.rw.user.Response> calle, retrofit2.Response<in.setone.pelayanansurat.model.rw.user.Response> responsee) {
                                        List<in.setone.pelayanansurat.model.rw.user.Result> data = new ArrayList<>();
                                        data.addAll(responsee.body().getResult());
                                        if (data.size() == 1){
                                            Preferences.setLoggedInStatus(getBaseContext(),true);
                                            Preferences.setRegisteredUser(getBaseContext(),data.get(0).getName());
                                            Preferences.setLoggedInNip(getBaseContext(),data.get(0).getNip());
                                            Preferences.setLoggedInRw(getBaseContext(),data.get(0).getCode());
                                            startOwnService();
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            getApplication().startActivity(intent);
                                            finish();
                                            btnLogin.setEnabled(true);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<in.setone.pelayanansurat.model.rw.user.Response> calle, Throwable te) {

                                        btnLogin.setEnabled(true);
                                    }
                                });
                            }
                        }else {
                            btnLogin.setEnabled(true);
                            Toast.makeText(getApplicationContext(), "NIP tidak terdaftar !!!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        btnLogin.setEnabled(true);
                        Toast.makeText(getApplicationContext(), "Gagal terhubung Jaringan !!!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
