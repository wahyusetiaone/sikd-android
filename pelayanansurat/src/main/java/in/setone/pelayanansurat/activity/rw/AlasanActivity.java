package in.setone.pelayanansurat.activity.rw;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import in.setone.pelayanansurat.R;
import in.setone.pelayanansurat.activity.rw.FormConfirmationActivity;
import in.setone.pelayanansurat.model.rw.update.Response;
import in.setone.pelayanansurat.network.rw.RetrofitService;
import in.setone.pelayanansurat.network.rw.ServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

public class AlasanActivity extends AppCompatActivity {

    private EditText alasan;
    private Button kirimTolak;
    private TextView tvback;
    private ImageView icback;
    private ServicesAPI servicesAPI;
    private String s_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alasan_both);
        icback = (ImageView) findViewById(R.id.icBack);
        tvback = (TextView) findViewById(R.id.tvBack);

        alasan = (EditText) findViewById(R.id.tvAlasanRt);
        kirimTolak = (Button) findViewById(R.id.btnTolakSuratRTAlasan);

        Intent intent = getIntent();
        s_id = intent.getStringExtra(FormConfirmationActivity.KEY_ID_TRANSAKSI);

        servicesAPI = RetrofitService.createServiceDefault(ServicesAPI.class);

        tvback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        icback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        kirimTolak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kirimTolak.setEnabled(false);
                String als = alasan.getText().toString();
                if (!als.isEmpty()) {
                    servicesAPI.updateStatusLetter(s_id, "W0", als).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            if (response.body().isStatus()) {
                                Toast.makeText(getBaseContext(), "Berhasil menolak surat surat !!!", Toast.LENGTH_SHORT).show();
                                redirect();
                                kirimTolak.setEnabled(true);
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {

                            kirimTolak.setEnabled(true);
                        }
                    });
                    redirect();
                }else {
                    kirimTolak.setEnabled(true);
                    Toast.makeText(AlasanActivity.this,"Alasan Penolakan surat tidak boleh kosong !!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void redirect() {
        Intent intent = new Intent(in.setone.pelayanansurat.activity.rw.AlasanActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}