package in.setone.pelayanansurat.activity.warga;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import in.setone.pelayanansurat.BuildConfig;
import in.setone.pelayanansurat.R;

public class WargaDownloadLetterActivity extends AppCompatActivity {

    private WebView webView;
    private Button btnUnduh;
    private String id,surat;
    private Button kembali;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_letterv2_warga);

        kembali = (Button) findViewById(R.id.btnKembali);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        try {

            Intent intent = getIntent();
            id = intent.getStringExtra("_id_transaksi");
            surat = intent.getStringExtra("jenis_surat");
            Log.d("DownloadLetterActivity", surat + " " + String.valueOf(id));

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("getStringExtra_EX", e + "");
        }

        webView = (WebView) findViewById(R.id.webView);
        btnUnduh = (Button) findViewById(R.id.btnUnduh);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
                btnUnduh.setVisibility(View.VISIBLE);
                //if page loaded successfully then show print button
//                findViewById(R.id.fab).setVisibility(View.VISIBLE);
            }
        });

        switch (surat){
            case "Surat Izin Hiburan":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_sip/"+ id);
                break;

            case "Surat Keterangan Tidak Mampu":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_sktm/"+ id);
                break;

            case "Surat Keterangan Kematian":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_skk/"+ id);
                break;

            case "Surat Keterangan Kelahiran":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_skl/"+ id);
                break;

            case "Surat Keterangan Domisili Usaha":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_skdu/"+ id);
                break;

            case "Surat Pengantar KTP":
                webView.loadUrl(BuildConfig.URL_WEB+"surat_skktp/"+ id);
                break;

        }

        btnUnduh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printPDF(view);
            }
        });

        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

    }

    //create a function to create the print job
    private void createWebPrintJob(WebView webView) {

        //create object of print manager in your device
        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

        //create object of print adapter
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

        //provide name to your newly generated pdf file
        String jobName = getString(R.string.app_name) + " Print Test";

        //open print dialog
        printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());
    }

    //perform click pdf creation operation on click of print button click
    public void printPDF(View view) {
        createWebPrintJob(webView);
    }
}
