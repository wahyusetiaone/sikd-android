package in.setone.pelayanansurat.activity.warga;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import in.setone.pelayanansurat.R;
import in.setone.pelayanansurat.model.warga.requestv2.Response;
import in.setone.pelayanansurat.network.warga.WargaRetrofitService;
import in.setone.pelayanansurat.network.warga.WargaServicesAPI;
import in.setone.pelayanansurat.utility.Helper;
import retrofit2.Call;
import retrofit2.Callback;

public class WargaFormSktmActivity extends AppCompatActivity {

    WargaServicesAPI servicesAPI;
    EditText kep, ayah, ibu, isi;
    Button sendRequest;
    int surat;

    private TextView tvback;
    private ImageView icback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_sktmv2_warga);
        icback = (ImageView) findViewById(R.id.icBack);
        tvback = (TextView) findViewById(R.id.tvBack);

        servicesAPI = WargaRetrofitService.createServiceDefault(WargaServicesAPI.class);

        sendRequest = (Button) findViewById(R.id.btnSendSktm);
        kep = (EditText) findViewById(R.id.ed_sktm_kep);
        ayah = (EditText) findViewById(R.id.ed_sktm_ayah);
        ibu = (EditText) findViewById(R.id.ed_sktm_ibu);
        isi = (EditText) findViewById(R.id.ed_sktm_isi);

        tvback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        icback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        final Intent intent = getIntent();
        surat = intent.getIntExtra("surat",0);

        sendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Helper.backgroundLoadChange(sendRequest, getApplicationContext(), true);
                String hsv = ayah.getText().toString()+'#'+ibu.getText().toString()+'#'+isi.getText().toString();
                Log.d("CEKK", hsv);
                if (!hsv.isEmpty()){
                    servicesAPI.requestv2(WargaPreferences.getLoggedInNIK(getBaseContext()), surat, kep.getText().toString(), hsv).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            if (response.body().isStatus()){
                                Helper.backgroundLoadChange(sendRequest, getApplicationContext(), true);
                                Toast.makeText(getApplicationContext(), "Surat berhasil di kirim !!!", Toast.LENGTH_SHORT).show();
                                Intent intent1 = new Intent(WargaFormSktmActivity.this, WargaMainActivity.class);
                                startActivity(intent1);
                                finish();
                            }else {
                                Helper.backgroundLoadChange(sendRequest, getApplicationContext(), true);
                                Toast.makeText(getApplicationContext(), "Surat gagal di kirim !!!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {
                            Helper.backgroundLoadChange(sendRequest, getApplicationContext(), true);
                        }
                    });
                }else {
                    Helper.backgroundLoadChange(sendRequest, getApplicationContext(), true);
                    Toast.makeText(WargaFormSktmActivity.this, "Tidak boleh ada yang Kosong !!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
