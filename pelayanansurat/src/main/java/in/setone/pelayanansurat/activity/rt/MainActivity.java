package in.setone.pelayanansurat.activity.rt;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import in.setone.pelayanansurat.R;
import in.setone.pelayanansurat.adapter.rt.LetterAdapter;
import in.setone.pelayanansurat.model.rt.history.Response;
import in.setone.pelayanansurat.model.rt.history.Result;
import in.setone.pelayanansurat.network.rt.RetrofitService;
import in.setone.pelayanansurat.network.rt.ServicesAPI;
import in.setone.pelayanansurat.service.rt.RtService;
import retrofit2.Call;
import retrofit2.Callback;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ServicesAPI servicesAPI;
    private RecyclerView recyclerView;
    private List<Result> data = new ArrayList<>();
    private LetterAdapter adapter;
    private TextView antrian, name;
    private ImageView logout;
    private Button history;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainv2_rt);

        if (!isMyServiceRunning(RtService.class)){
            startService(new Intent(MainActivity.this, RtService.class));
        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        name = (TextView) findViewById(R.id.tvName);
        antrian = (TextView) findViewById(R.id.tvAntrian);
        history = (Button) findViewById(R.id.historyLetter);
        logout = (ImageView) findViewById(R.id.logout);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.rcLetter);

        adapter = new LetterAdapter(data);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HistoryActivity.class);
                startActivity(intent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.clearLoggedInUser(getBaseContext());
                stopService(new Intent(MainActivity.this, RtService.class));
                Intent intent = new Intent(MainActivity.this, in.setone.pelayanansurat.MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        name.setText(Preferences.getRegisteredUser(this));
        Log.d("MainActivity", String.valueOf(Preferences.getLoggedInRt(this)));

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                loadRecyclerViewData();
            }
        });
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRefresh() {
        loadRecyclerViewData();
    }

    private void loadRecyclerViewData(){
        // Showing refresh animation before making http call
        mSwipeRefreshLayout.setRefreshing(true);

        servicesAPI = RetrofitService.createServiceDefault(ServicesAPI.class);
        servicesAPI.letterHistoryWaiting(Preferences.getLoggedInRt(getBaseContext()),Preferences.getLoggedInRw(getBaseContext())).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.body().isStatus()){
                    if (!response.body().getResult().isEmpty()){
                        data.clear();
                        data.addAll(response.body().getResult());
                        Log.d("MainActivity", data.toString());
                        adapter.notifyDataSetChanged();
                        antrian.setVisibility(View.INVISIBLE);
                    }else {
                        antrian.setVisibility(View.VISIBLE);
                    }
                }else {
                    antrian.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

                // Stopping swipe refresh
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                Log.e("MainActivity", t.getMessage());
                Toast.makeText(getApplicationContext(), "Gagal terhubung Server !!!",Toast.LENGTH_SHORT).show();
            }
        });


    }
}
