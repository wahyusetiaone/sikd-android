package in.setone.pelayanansurat.network.rw;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicesAPI {

    @FormUrlEncoded
    @POST("login")
    Call<in.setone.pelayanansurat.model.rw.login.Response> login(
            @Field("nip") String nip
    );

    @FormUrlEncoded
    @POST("get")
    Call<in.setone.pelayanansurat.model.rw.user.Response> userInfomation(
            @Field("nip") String nip
    );

    @FormUrlEncoded
    @POST("history")
    Call<in.setone.pelayanansurat.model.rw.history.Response> letterHistory(
            @Field("rw") int rw
    );
    @FormUrlEncoded
    @POST("historyWaiting")
    Call<in.setone.pelayanansurat.model.rw.history.Response> letterHistoryWaiting(
            @Field("rw") int rw
    );

    @FormUrlEncoded
    @POST("update")
    Call<in.setone.pelayanansurat.model.rw.update.Response> updateStatusLetter(
            @Field("id_transaksi") String id_transaksi,
            @Field("status") String status,
            @Field("alasan") String alasan
    );
}
