package in.setone.pelayanansurat.adapter.rw;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.pelayanansurat.R;
import in.setone.pelayanansurat.model.rw.history.Result;

import java.util.List;

public class LetterHistoryAdapter extends RecyclerView.Adapter<LetterHistoryAdapter.LetterViewHolder> {

    private List<Result> data;

    public LetterHistoryAdapter(List<Result> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public LetterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_list_letter_rw, parent, false);
        return new LetterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LetterViewHolder holder, int position) {
        final Result item = data.get(position);
        holder.name.setText(item.getName());
        holder.surat.setText(item.getNama_surat());
        String str = item.getKeperluan();
        String kep = str;
        if (str.length() > 25 ){
            kep = str.substring(0,25) + " ...";
        }
        holder.keperluan.setText(kep);
        Resources res = holder.itemView.getResources();
        switch (item.getStatus()) {
            case "1":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_check_circle_outline));
                break;
            case "R0":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_highlight_off));
                break;
            case "W0":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_highlight_off));
                break;
            case "W":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_access_time));
                break;
            case "K0":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_highlight_off));
                break;
            case "K":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_access_time));
                break;
        }


//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(holder.itemView.getContext(), FormConfirmationActivity.class);
//                intent.putExtra("_id_transaksi", String.valueOf(item.get_id_transaksi()));
//                intent.putExtra("kk", String.valueOf(item.getKk()));
//                intent.putExtra("nik", String.valueOf(item.getNik()));
//                intent.putExtra("nama", String.valueOf(item.getName()));
//                intent.putExtra("alamat", String.valueOf(item.getAddress()));
//                intent.putExtra("tempatL", "Karanganyar");
//                intent.putExtra("tanggalL", String.valueOf(item.getBirthday()));
//                intent.putExtra("jenis_kelamin", String.valueOf(item.getGender()));
//                intent.putExtra("jenis_surat", String.valueOf(item.getNama_surat()));
//                intent.putExtra("keperluan", String.valueOf(item.getKeperluan()));
//
//                holder.itemView.getContext().startActivity(intent);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return (data != null) ? data.size() : 0;
    }

    public class LetterViewHolder extends RecyclerView.ViewHolder {
        private TextView name,surat, keperluan;
        private ImageView imgWait;
        public LetterViewHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tvNameLetterr);
            surat = (TextView) itemView.findViewById(R.id.tvJenisLetter);
            keperluan = (TextView) itemView.findViewById(R.id.tvKeperluan);
            imgWait = (ImageView) itemView.findViewById(R.id.imgWait);
        }
    }
}
