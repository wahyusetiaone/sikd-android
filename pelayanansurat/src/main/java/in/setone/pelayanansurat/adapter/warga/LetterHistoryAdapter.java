package in.setone.pelayanansurat.adapter.warga;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.pelayanansurat.R;
import in.setone.pelayanansurat.activity.warga.WargaDownloadLetterActivity;
import in.setone.pelayanansurat.model.warga.history.Result;

import java.util.List;

public class LetterHistoryAdapter extends RecyclerView.Adapter<LetterHistoryAdapter.LetterViewHolder> {


    private List<Result> data;
    private Activity activity;

    public LetterHistoryAdapter(List<Result> data, Activity activity) {
        this.data = data;
        this.activity = activity;
    }

    @NonNull
    @Override
    public LetterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_list_letter_warga, parent, false);
        return new LetterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LetterViewHolder holder, int position) {
        final Result item = data.get(position);
        holder.name.setText(item.getName());
        holder.surat.setText(item.getNama_surat());

        String str = item.getKeperluan();
        String kep = str;
        if (str.length() > 25) {
            kep = str.substring(0, 25) + " ...";
        }
        holder.keperluan.setText(kep);
        Resources res = holder.itemView.getResources();

        String st = "";
        int color = 0;
        switch (item.getStatus()) {
            case "1":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_check_circle_outline));
                st = "Diterbitkan";
                color = R.color.success;
                break;
            case "R":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_access_time));
                st = "Menunggu persetujuan RT";
                color = R.color.waiting;
                break;
            case "R0":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_highlight_off));
                st = "Ditolak RT";
                color = R.color.failed;
                break;
            case "W0":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_highlight_off));
                st = "Ditolak RW";
                color = R.color.failed;
                break;
            case "W":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_access_time));
                st = "Menunggu persetujuan RW";
                color = R.color.waiting;
                break;
            case "K0":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_highlight_off));
                st = "Ditolak kelurahan";
                color = R.color.failed;
                break;
            case "K":
                holder.imgWait.setImageDrawable(res.getDrawable(R.drawable.ic_access_time));
                st = "Menunggu persetujuan Kelurahan";
                color = R.color.waiting;
                break;
        }
        final String stu = st;
        holder.status.setText(st);
        holder.status.setTextColor(color);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.getStatus().equals("1")) {
                    Intent intent = new Intent(holder.itemView.getContext(), WargaDownloadLetterActivity.class);
                    intent.putExtra("_id_transaksi", String.valueOf(item.get_id_transaksi()));
                    intent.putExtra("kk", String.valueOf(item.getKk()));
                    intent.putExtra("nik", String.valueOf(item.getNik()));
                    intent.putExtra("nama", String.valueOf(item.getName()));
                    intent.putExtra("alamat", String.valueOf(item.getAddress()));
                    intent.putExtra("tempatL", "Karanganyar");
                    intent.putExtra("tanggalL", String.valueOf(item.getBirthday()));
                    intent.putExtra("jenis_kelamin", String.valueOf(item.getGender()));
                    intent.putExtra("jenis_surat", String.valueOf(item.getNama_surat()));
                    intent.putExtra("keperluan", String.valueOf(item.getKeperluan()));

                    holder.itemView.getContext().startActivity(intent);
                } else if (item.getStatus().equals("R0") || item.getStatus().equals("W0")) {
                    showDialogTolak(holder.itemView, item.getAlasan());
                } else {
                    Toast.makeText(holder.itemView.getContext(), "Status surat " + stu, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showDialogTolak(View itemView, String alasan) {
        AlertDialog alertDialog = new AlertDialog.Builder(itemView.getContext())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alasan Penolakan Surat")
                .setMessage(alasan)
                .setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .show();
    }

    @Override
    public int getItemCount() {
        return (data != null) ? data.size() : 0;
    }

    public class LetterViewHolder extends RecyclerView.ViewHolder {
        private TextView name, surat, status, keperluan;
        private ImageView imgWait;

        public LetterViewHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tvNameLetterr);
            surat = (TextView) itemView.findViewById(R.id.tvJenisLetter);
            keperluan = (TextView) itemView.findViewById(R.id.tvKeperluan);
            status = (TextView) itemView.findViewById(R.id.tvStatus);
            imgWait = (ImageView) itemView.findViewById(R.id.imgWait);
        }
    }
}
