package in.setone.apprt.activity.rt;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    static final String KEY_USER_TEREGISTER ="user";
    static final String KEY_RT_SEDANG_LOGIN ="Rt_logged_in";
    static final String KEY_RW_SEDANG_LOGIN ="Rw_logged_in";
    static final String KEY_NIP_SEDANG_LOGIN = "Nip_logged_in";
    static final String KEY_STATUS_SEDANG_LOGIN = "Status_logged_in";

    private static SharedPreferences getSharedPreference(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setRegisteredUser(Context context, String name){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_TEREGISTER, name);
        editor.apply();
    }
    public static String getRegisteredUser(Context context){
        return getSharedPreference(context).getString(KEY_USER_TEREGISTER,"");
    }

    public static void setLoggedInRt(Context context, int rt){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(KEY_RT_SEDANG_LOGIN, rt);
        editor.apply();
    }
    public static int getLoggedInRt(Context context){
        return getSharedPreference(context).getInt(KEY_RT_SEDANG_LOGIN,0);
    }

    public static void setLoggedInRw(Context context, int rt){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(KEY_RW_SEDANG_LOGIN, rt);
        editor.apply();
    }
    public static int getLoggedInRw(Context context){
        return getSharedPreference(context).getInt(KEY_RW_SEDANG_LOGIN,0);
    }

    public static void setLoggedInNip(Context context, String nip){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_NIP_SEDANG_LOGIN, nip);
        editor.apply();
    }
    public static String getLoggedInNip(Context context){
        return getSharedPreference(context).getString(KEY_NIP_SEDANG_LOGIN,"");
    }

    public static void setLoggedInStatus(Context context, boolean status){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(KEY_STATUS_SEDANG_LOGIN,status);
        editor.apply();
    }
    public static boolean getLoggedInStatus(Context context){
        return getSharedPreference(context).getBoolean(KEY_STATUS_SEDANG_LOGIN,false);
    }

    public static void clearLoggedInUser (Context context){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.remove(KEY_NIP_SEDANG_LOGIN);
        editor.remove(KEY_STATUS_SEDANG_LOGIN);
        editor.apply();
    }
}