package in.setone.apprt.activity.rt;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import in.setone.apprt.R;
import in.setone.apprt.model.rt.update.Response;
import in.setone.apprt.network.rt.RetrofitService;
import in.setone.apprt.network.rt.ServicesAPI;
import retrofit2.Call;
import retrofit2.Callback;

public class FormConfirmationActivity extends AppCompatActivity {

    private TextView kk,nik,nama,alamat, jenis_kelamin, jenis_surat, keperluan;
    private String s_id,skk,snik,snama,salamat, stempatL,stanggalL, sjenis_kelamin, sjenis_surat, skeperluan;
    private Button btnSetuju, btnTolak;
    private ServicesAPI servicesAPI;
    private TextView tvback;
    private ImageView icback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_confirmationv2_rt);

        Intent intent = getIntent();
        s_id = intent.getStringExtra("_id_transaksi");
        skk = intent.getStringExtra("kk");
        snik = intent.getStringExtra("nik");
        snama = intent.getStringExtra("nama");
        salamat = intent.getStringExtra("alamat");
        sjenis_kelamin = intent.getStringExtra("jenis_kelamin");
        sjenis_surat = intent.getStringExtra("jenis_surat");
        skeperluan = intent.getStringExtra("keperluan");

        kk = (TextView) findViewById(R.id.tKK);
        nik = (TextView) findViewById(R.id.tNIK);
        nama = (TextView) findViewById(R.id.tNama);
        alamat = (TextView) findViewById(R.id.tAlamat);
        jenis_kelamin = (TextView) findViewById(R.id.tJenisKelamin);
        jenis_surat = (TextView) findViewById(R.id.tJenisSurat);
        keperluan = (TextView) findViewById(R.id.tKeperluan);

        icback = (ImageView) findViewById(R.id.icBack);
        tvback = (TextView) findViewById(R.id.tvBack);
        btnSetuju = (Button) findViewById(R.id.btnSetuju);
        btnTolak = (Button) findViewById(R.id.btnTolak);

        kk.setText(skk);
        nik.setText(snik);
        nama.setText(snama);
        alamat.setText(salamat);
        jenis_kelamin.setText(sjenis_kelamin);
        jenis_surat.setText(sjenis_surat);
        keperluan.setText(skeperluan);

        servicesAPI = RetrofitService.createServiceDefault(ServicesAPI.class);

        btnSetuju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicesAPI.updateStatusLetter(s_id, "W").enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        if (response.body().isStatus()){
                            Toast.makeText(getBaseContext(), "Berhasil menyetujui surat !!!", Toast.LENGTH_SHORT).show();
                            redirect();
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {

                    }
                });
                redirect();
            }
        });

        btnTolak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                servicesAPI.updateStatusLetter(s_id, "R0").enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        if (response.body().isStatus()){
                            Toast.makeText(getBaseContext(), "Berhasil menolak surat surat !!!", Toast.LENGTH_SHORT).show();
                            redirect();
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {

                    }
                });
                redirect();
            }
        });

        tvback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        icback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
    }

    private void redirect(){
        Intent intent = new Intent(FormConfirmationActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
