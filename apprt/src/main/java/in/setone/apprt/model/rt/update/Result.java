package in.setone.apprt.model.rt.update;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

//    "id_transaksi": "10001",
//            "status": "K"
    @SerializedName("id_transaksi")
    @Expose
    private String id_transaksi;

    @SerializedName("status")
    @Expose
    private String status;

    public Result(String id_transaksi, String status) {
        this.id_transaksi = id_transaksi;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id_transaksi='" + id_transaksi + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
