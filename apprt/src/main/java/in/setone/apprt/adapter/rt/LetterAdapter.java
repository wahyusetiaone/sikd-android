package in.setone.apprt.adapter.rt;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.apprt.activity.rt.FormConfirmationActivity;
import in.setone.apprt.R;
import in.setone.apprt.model.rt.history.Result;

import java.util.List;

public class LetterAdapter extends RecyclerView.Adapter<LetterAdapter.LetterViewHolder> {

    private List<Result> data;

    public LetterAdapter(List<Result> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public LetterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_list_letter_rt, parent, false);
        return new LetterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LetterViewHolder holder, int position) {
        final Result item = data.get(position);
        holder.name.setText(item.getName());
        holder.surat.setText(item.getNama_surat());
        String str = item.getKeperluan();
        String kep = str;
        if (str.length() > 25 ){
            kep = str.substring(0,25) + " ...";
        }
        holder.keperluan.setText(kep);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(holder.itemView.getContext(), FormConfirmationActivity.class);
                intent.putExtra("_id_transaksi", String.valueOf(item.get_id_transaksi()));
                intent.putExtra("kk", String.valueOf(item.getKk()));
                intent.putExtra("nik", String.valueOf(item.getNik()));
                intent.putExtra("nama", String.valueOf(item.getName()));
                intent.putExtra("alamat", String.valueOf(item.getAddress()));
                intent.putExtra("tempatL", "Karanganyar");
                intent.putExtra("tanggalL", String.valueOf(item.getBirthday()));
                intent.putExtra("jenis_kelamin", String.valueOf(item.getGender()));
                intent.putExtra("jenis_surat", String.valueOf(item.getNama_surat()));
                intent.putExtra("keperluan", String.valueOf(item.getKeperluan()));

                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (data != null) ? data.size() : 0;
    }

    public class LetterViewHolder extends RecyclerView.ViewHolder {
        private TextView name,surat,keperluan;
        public LetterViewHolder(@NonNull View rootView) {
            super(rootView);
//            itemView = (CardView) itemView.findViewById(R.id.itemLetter);
            name = (TextView) rootView.findViewById(R.id.tvNameLetterr);
            surat = (TextView) rootView.findViewById(R.id.tvJenisLetter);
            keperluan = (TextView) rootView.findViewById(R.id.tvKeperluan);
        }
    }
}
