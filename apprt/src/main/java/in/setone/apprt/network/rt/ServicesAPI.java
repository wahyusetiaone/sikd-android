package in.setone.apprt.network.rt;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicesAPI {

    @FormUrlEncoded
    @POST("login")
    Call<in.setone.apprt.model.rt.login.Response> login(
            @Field("nip") String nip
    );

    @FormUrlEncoded
    @POST("get")
    Call<in.setone.apprt.model.rt.user.Response> userInfomation(
            @Field("nip") String nip
    );

    @FormUrlEncoded
    @POST("history")
    Call<in.setone.apprt.model.rt.history.Response> letterHistory(
            @Field("rt") int rt,
            @Field("rw") int rw
    );
    @FormUrlEncoded
    @POST("historyWaiting")
    Call<in.setone.apprt.model.rt.history.Response> letterHistoryWaiting(
            @Field("rt") int rt,
            @Field("rw") int rw
    );

    @FormUrlEncoded
    @POST("update")
    Call<in.setone.apprt.model.rt.update.Response> updateStatusLetter(
            @Field("id_transaksi") String id_transaksi,
            @Field("status") String status
    );
}
